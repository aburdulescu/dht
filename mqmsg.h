#ifndef __DHT_MQMSG__
#define __DHT_MQMSG__

/**
 * @file mqmsg.h
 */

enum dht_mqmsg_type
{
    DHT_MQMSG_TYPE_UNKNOWN,
    // from main thread
    DHT_MQMSG_TYPE_FIND_NODE,
    DHT_MQMSG_TYPE_STOP,
    // from rpc thread
    DHT_MQMSG_TYPE_RTABLE_UPDATE,
    DHT_MQMSG_TYPE_RTABLE_FIND_K_CLOSEST,
    // from rtable thread
    DHT_MQMSG_TYPE_PING,
    DHT_MQMSG_TYPE_FIND_NODE,
};

struct dht_mqmsg
{
    void *data;
    enum dht_mqmsg_type type;
};

struct dht_mqmsg* dht_mqmsg_new(enum dht_mqmsg_type type, void *data);
void dht_mqmsg_delete(struct dht_mqmsg *m);

#endif
