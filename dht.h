#ifndef __DHT__
#define __DHT__

/**
 * @file dht.h
 */

#include <pthread.h>

#include "rtable.h"
#include "rpc.h"
#include "mq.h"
#include "th_rtable.h"
#include "th_rpc.h"


struct dht
{
    struct dht_rtable rt;

    struct dht_rpc rpc;

    struct dht_thread_rtable th_rtable;
    struct dht_thread_rpc th_rpc;

    struct mq mq;
};

/**
 * @brief Initialize dht structure
 * @param dht a stack allocated dht structure
 * @param id_string a string that will be hashed and used as a the node id
 * @param port the port on which this dht node will receive requests from other nodes
 */
void dht_new(struct dht *dht, unsigned char *id_string, int port);

/**
 * Free resources used by dht structure
 */
void dht_delete(struct dht *dht);

/**
 * Start bootstrap process with dht node at addr
 */
int dht_bootstrap(struct dht *dht, const struct dht_netaddr *bootstrap_addr);

/**
 * Query the dht for the node with nodeid=id
 */
int dht_find_node(const struct dht *dht, const dht_nodeid_t id, struct dht_contact *c);

/**
 * Start dht thread(runs a loop that receives and responds to requests from other nodes)
 */
void dht_start(struct dht *dht);

/**
 * Stop dht thread
 */
void dht_stop(struct dht *dht);

/**
 * Save the routing table of the dht structure as JSON to the file at filepath
 */
void dht_save(const struct dht *dht, const char *filepath);

/**
 * Load the routing table of the dht structure from the file at filepath
 */
void dht_load(const struct dht *dht, const char *filepath);

#endif
