#include "queue.h"

#include <stdlib.h>

static struct queue* new_node(void *data, struct queue *next);

struct queue* queue_init()
{
    return NULL;
}

void queue_delete(struct queue *q)
{
    struct queue *i = q;
    while(i != NULL)
    {
        struct queue *tmp = i;
        i = i->next;
        free(tmp);
    }
}

void queue_push(struct queue **q, void* data)
{
    struct queue *tmp = new_node(data, *q);
    if(*q == NULL)
        *q = tmp;
    else
        (*q)->next = tmp;
}

void* queue_pop(struct queue **q)
{
    if(queue_is_empty(*q))
        return NULL;

    struct queue *tmp = *q;
    *q = (*q)->next;
    void* popped = tmp->data;
    free(tmp);

    return popped;
}

void* queue_peek(struct queue *q)
{
    if(queue_is_empty(q))
        return 0;
    return q->data;
}

int queue_is_empty(struct queue *q)
{
    return !q;
}

struct queue* new_node(void *data, struct queue *next)
{
    struct queue *tmp;
    tmp = (struct queue*)malloc(sizeof(struct queue));
    if(!tmp)
        exit(0);
    tmp->data = data;
    tmp->next = next;
    return tmp;
}
