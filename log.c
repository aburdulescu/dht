#include "log.h"

#include <string.h>
#include <sys/syscall.h>
#include <libgen.h>


void dht_log_open(char *progname)
{
    setlogmask(LOG_UPTO(LOG_DEBUG));
    openlog(progname, LOG_CONS | LOG_PID, LOG_USER);
}

void dht_log_close()
{
    closelog();
}

void dht_log(int level, const char *file, const char *func, const char *format, ...)
{
    char prefix[] = "[%d]%s:%s -  ";

    char fmt[strlen(prefix)+strlen(format)];
    memset(fmt, 0, sizeof(fmt));

    strcpy(fmt, prefix);
    strcat(fmt, format);

    va_list ap;
    va_start(ap, format);
    syslog(level, fmt, syscall(SYS_gettid), basename(file), func, ap);
    va_end(ap);
}
