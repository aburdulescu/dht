#ifndef __DHT_RSPMAP__
#define __DHT_RSPMAP__

#include "hash.h"


typedef unsigned int dht_rspmap_timeout_t;

enum dht_rspmap_status_e
{
    RSPMAP_STATUS_OK,
    RSPMAP_STATUS_TIMEOUT,
};
typedef enum dht_rspmap_status_e dht_rspmap_status_t;

typedef void (*dht_rspmap_cb_t)(void *res, dht_rspmap_status_t status);

struct dht_rspmap_entry_s
{
    struct dht_rspmap_entry_s *next;
    dht_hash_t *hash;
    dht_rspmap_cb_t cb;
    unsigned long timestamp; // when was added
    unsigned int timeout; // max num. of seconds that can pass until the rsp arrives
};
typedef struct dht_rspmap_entry_s dht_rspmap_entry_t;

struct dht_rspmap_s
{
    struct dht_rspmap_entry_s *map;
    int len;
};
typedef struct dht_rspmap_s dht_rspmap_t;


dht_rspmap_t* dht_rspmap_new();
void dht_rspmap_delete(dht_rspmap_t *m);
void dht_rspmap_insert(dht_rspmap_t **m,
                       dht_hash_t *hash,
                       dht_rspmap_cb_t cb,
                       dht_rspmap_timeout_t timeout);
dht_rspmap_entry_t* dht_rspmap_get(dht_rspmap_t *m, dht_hash_t *h);
void dht_rspmap_print(dht_rspmap_t *m);
#endif
