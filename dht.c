#include "dht.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "netaddr.h"
#include "mqmsg.h"
#include "log.h"


void dht_new(struct dht *dht, unsigned char *id_string, int port)
{
    DHT_LOG(LOG_DEBUG, "in");

    dht_nodeid_t id;
    dht_nodeid_new(id, (unsigned char *)id_string, strlen(id_string));

    dht_rtable_new(&dht->rt, id);

    dht_rpc_open(&dht->rpc, port);
    if(dht->rpc.fd == -1)
    {
        DHT_LOG(LOG_WARNING, "dht_rpc_open failed");
    }

    mq_init(&dht->mq);
}

void dht_delete(struct dht *dht)
{
    DHT_LOG(LOG_DEBUG, "in");

    dht_rtable_delete(&dht->rt);
    dht_rpc_close(&dht->rpc);

    mq_delete(&dht->mq);
}

// TODO: this searches the id in the node rt; add full search; i.e. query dht
// TODO: something is wrong here
int dht_find_node(const struct dht *dht, const dht_nodeid_t id, struct dht_contact *c)
{
    DHT_LOG(LOG_DEBUG, "in");
    dht_nodeid_t xor;
    dht_nodeid_xor(dht->rt.id, id, xor);
    int kb_idx = dht_nodeid_prefixLength(xor);

    struct dht_kbucket_node *kb_node = dht_kbucket_find(dht->rt.kbuckets[kb_idx], id);
    if(kb_node != NULL)
    {
        memcpy(c, &kb_node->contact, sizeof(struct dht_contact));
        return 0;
    }
    else
    {
        // query dht
        return -1;
    }
}

int dht_bootstrap(struct dht *dht, const struct dht_netaddr *bootstrap_addr)
{
    DHT_LOG(LOG_DEBUG, "in");
    dht_clist_t contacts;
    memset(contacts, 0, sizeof(contacts));

    int rc = dht_rpc_find_node(&dht->rpc,
                               dht->rt.id,
                               bootstrap_addr,
                               dht->rt.id,
                               contacts);
    if(rc == -1)
        return -1;

    for(int i = 0; i < DHT_CLIST_LENGTH; ++i)
    {
        if(dht_contact_is_empty(&contacts[i].contact) == 0)
        {
            rc = dht_rpc_ping(&dht->rpc,
                              dht->rt.id,
                              &contacts[i].contact.addr);
            if(rc == 0)
            {
                DHT_LOG(LOG_DEBUG, "[%s:%d]is alive\n",
                       contacts[i].contact.addr.ip,
                       contacts[i].contact.addr.port);
                dht_rtable_update(&dht->rt, &contacts[i].contact);
            }
            else
            {
                DHT_LOG(LOG_DEBUG, "[%s:%d]is not alive\n",
                       contacts[i].contact.addr.ip,
                       contacts[i].contact.addr.port);
            }
        }
        else
        {
            DHT_LOG(LOG_DEBUG, "contact %d is empty => ignoring\n", i);
        }
    }

    // now add the bootstrap node
    /* struct dht_contact sender; */
    /* dht_nodeid_copy(sender.id, m_rsp.header.sender); */
    /* memcpy(&sender.addr, &bootstrap_addr, sizeof(bootstrap_addr)); */
    /* dht_update(dht, &sender); */

    dht_rtable_print(&dht->rt);
    // TODO: send find to nodes given by bootstrap node

    return 0;
}

void dht_start(struct dht *dht)
{
    DHT_LOG(LOG_DEBUG, "in");

    dht_thread_rtable_start(&dht->th_rtable, dht);
    dht_thread_rpc_start(&dht->th_rpc, dht);
}

void dht_stop(struct dht *dht)
{
    DHT_LOG(LOG_DEBUG, "in");

    dht_thread_rtable_stop(&dht->th_rtable);
    dht_thread_rpc_stop(&dht->th_rpc);

    DHT_LOG(LOG_DEBUG, "Waiting for threads to finish ...");
    int stop_responses = 0;
    while(stop_responses != 2)
    {
        DHT_LOG(LOG_DEBUG, "Thread %d responded to stop message.\n", stop_responses+1);
        struct dht_mqmsg *m = mq_wait(&dht->mq);
        if(m->type == DHT_MQMSG_TYPE_STOP)
            stop_responses++;
        dht_mqmsg_delete(m);
    }
    DHT_LOG(LOG_DEBUG, "All threads are done. Bye!\n");

    dht_thread_rtable_deinit(&dht->th_rtable);
    dht_thread_rpc_deinit(&dht->th_rpc);
}

void dht_save(const struct dht *dht, const char *filepath)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct json *jo = dht_rtable_dump(&dht->rt);
    char *jo_string = json_dump(jo, 4);
    FILE *fp = fopen(filepath, "w");

    fprintf(fp, "%s", jo_string);

    fclose(fp);
    free(jo_string);
    json_delete(jo);
}
