#include "th_rpc.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>

#include "dht.h"
#include "mqmsg.h"
#include "log.h"


#define DHT_THREAD_RPC_PIPENAME "/tmp/mycdhtpipe"


static void* run(void *args);

static void on_rpcmsg(struct dht *dht);
static void on_rpcmsg_ping_req(struct dht *dht, struct dht_rpc_message *m_in, struct dht_netaddr *addr);
static void on_rpcmsg_find_node_req(struct dht *dht, struct dht_rpc_message *m_in, struct dht_netaddr *addr);

static int on_mqmsg(struct dht *dht);
static void on_mqmsg_stop(struct dht *dht);

static void readpipe(int fd);

void dht_thread_rpc_start(struct dht_thread_rpc *t, void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    int rc = mkfifo(DHT_THREAD_RPC_PIPENAME, 0666);
    assert(rc == 0);

    t->pipefd = open(DHT_THREAD_RPC_PIPENAME, O_RDWR);
    assert(t->pipefd > 0);

    dht_thread_init(&t->t, run, args);
}

void dht_thread_rpc_stop(struct dht_thread_rpc *t)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_STOP, NULL);
    mq_push(&t->t.mq, m);
    dht_thread_rpc_mqwakeup(t);
}

void dht_thread_rpc_deinit(struct dht_thread_rpc *t)
{
    dht_thread_deinit(&t->t);

    close(t->pipefd);
    unlink(DHT_THREAD_RPC_PIPENAME);
}

void dht_thread_rpc_mqwakeup(struct dht_thread_rpc *t)
{
    DHT_LOG(LOG_DEBUG, "in");
    //dummy write
    char buf[] = "dummy";
    write(t->pipefd, buf, strlen(buf));
}

//---------------------
static void* run(void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht *dht = (struct dht *)args;

    int exit_needed = 0;
    while(!exit_needed)
    {
        fd_set rfds;
        FD_ZERO(&rfds);

        FD_SET(dht->rpc.fd, &rfds);
        FD_SET(dht->th_rpc.pipefd, &rfds);

        int nfds = (dht->rpc.fd > dht->th_rpc.pipefd) ? dht->rpc.fd : dht->th_rpc.pipefd;

        int rc = select(nfds+1, &rfds, NULL, NULL, NULL);
        assert(rc > 0);

        if(FD_ISSET(dht->rpc.fd, &rfds))
        {
            on_rpcmsg(dht);
        }
        if(FD_ISSET(dht->th_rpc.pipefd, &rfds))
        {
            exit_needed = on_mqmsg(dht);
        }
    }

    pthread_exit(NULL);
}

static void on_rpcmsg(struct dht *dht)
{
    struct dht_rpc_message m_in;
    struct dht_netaddr addr;

    int rc = dht_rpc_read(&dht->rpc, &m_in, &addr);
    assert(rc == 0); // for now

    switch(m_in.type) {
    case DHT_RPC_MESSAGE_TYPE_PING_REQ:
    {
        on_rpcmsg_ping_req(dht, &m_in, &addr);
        break;
    }
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ:
    {
        on_rpcmsg_find_node_req(dht, &m_in, &addr);
        break;
    }
    default:
        break;
    }
}

static void on_rpcmsg_ping_req(struct dht *dht, struct dht_rpc_message *m_in, struct dht_netaddr *addr)
{
    DHT_LOG(LOG_DEBUG, "PING_REQ from %s:%d", addr->ip, addr->port);

    int rc = dht_rpc_ping_rsp(&dht->rpc, dht->rt.id, addr);
    assert(rc == 0);

    DHT_LOG(LOG_DEBUG, "PING_RSP to %s:%d", addr->ip, addr->port);

    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_RTABLE_UPDATE, m_in->sender);
    mq_push(&dht->th_rtable.t.mq, m);
}

static void on_rpcmsg_find_node_req(struct dht *dht, struct dht_rpc_message *m_in, struct dht_netaddr *addr)
{
    DHT_LOG(LOG_DEBUG, "FIND_NODE_REQ from %s:%d", addr->ip, addr->port);
    dht_nodeid_t wanted_id;
    dht_nodeid_copy(wanted_id, m_in->data.id);

    dht_nodeid_printable_t wanted_id_printable;
    dht_nodeid_btop(wanted_id, wanted_id_printable);
    DHT_LOG(LOG_DEBUG, "need k closest nodes to %s", wanted_id_printable);

    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_RTABLE_FIND_K_CLOSEST, wanted_id);

    mq_push(&dht->th_rtable.t.mq, m);
    m = mq_wait(&dht->th_rtable.t.mq);

    dht_clist_t contacts;
    memcpy(contacts, m->data, sizeof(contacts));
    dht_mqmsg_delete(m);

    int rc = dht_rpc_find_node_rsp(&dht->rpc, dht->rt.id, addr, contacts);
    assert(rc == 0);

    DHT_LOG(LOG_DEBUG, "FIND_NODE_RSP to %s:%d", addr->ip, addr->port);

    m = dht_mqmsg_new(DHT_MQMSG_TYPE_RTABLE_UPDATE, m_in->sender);
    mq_push(&dht->th_rtable.t.mq, m);
}

static int on_mqmsg(struct dht *dht)
{
    int exit_needed = 0;

    readpipe(dht->th_rpc.pipefd);

    struct dht_mqmsg *m = mq_pop(&dht->th_rpc.t.mq);

    switch(m->type) {
    case DHT_MQMSG_TYPE_STOP:
    {
        on_mqmsg_stop(dht);
        exit_needed = 1;
        break;
    }
    default:
        break;
    }

    return exit_needed;
}

static void on_mqmsg_stop(struct dht *dht)
{
    DHT_LOG(LOG_DEBUG, "STOP received");
    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_STOP, NULL);
    mq_push(&dht->mq, m);
}

static void readpipe(int fd)
{
    DHT_LOG(LOG_DEBUG, "in");
    //dummy read
    char buf[10];
    memset(buf, 0, 10);
    read(fd, buf, 10);
}
