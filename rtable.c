#include "rtable.h"

#include "log.h"


void dht_rtable_new(struct dht_rtable *rt, const dht_nodeid_t id)
{
    dht_nodeid_copy(rt->id, id);
    for(int i = 0; i < DHT_ROUTING_TABLE_SIZE; ++i)
    {
        struct dht_kbucket *kb = dht_kbucket_new();
        if(kb == NULL)
            return;
        rt->kbuckets[i] = kb;
    }
}

void dht_rtable_find_k_closest(const struct dht_rtable *rt, const dht_nodeid_t id, dht_clist_t contacts)
{
    dht_nodeid_t xor;
    dht_nodeid_xor(rt->id, id, xor);
    int kb_idx = dht_nodeid_prefixLength(xor);
    struct dht_kbucket *kb = rt->kbuckets[kb_idx];

    memset(contacts, 0, sizeof(dht_clist_t));

    // add all nodes from this kbucket
    int contacts_sz = 0;
    dht_clist_copy(contacts, &contacts_sz, kb, id);

    if(contacts_sz < DHT_K_VALUE)
    {
        // didn't find k nodes in this kbucket => fill the remaining places from neighbouring kbuckets
        for(int i = 1;
            ((kb_idx - i) >= 0 || (kb_idx + i) < DHT_ROUTING_TABLE_SIZE)
                && contacts_sz <= DHT_K_VALUE;
            ++i)
        {
            if((kb_idx - i) >= 0)
            {
                kb = rt->kbuckets[kb_idx - i];
                dht_clist_copy(contacts, &contacts_sz, kb, id);
            }
            if((kb_idx + i) < DHT_ROUTING_TABLE_SIZE)
            {
                kb = rt->kbuckets[kb_idx + i];
                dht_clist_copy(contacts, &contacts_sz, kb, id);
            }
        }
    }

    // we have k contacts, now sort contacts by distance to target id
    dht_clist_sort(contacts, contacts_sz);
}

void dht_rtable_delete(const struct dht_rtable *rt)
{
    DHT_LOG(LOG_DEBUG, "in");
    (void)rt;
}

struct json* dht_rtable_dump(const struct dht_rtable *rt)
{
    struct json *root_obj = json_new(JSON_TYPE_OBJECT);

    // id
    dht_nodeid_printable_t id_printable;
    dht_nodeid_btop(rt->id, id_printable);
    json_new_set_add(root_obj, JSON_TYPE_STRING, "id", 2);
    json_new_set_add(root_obj, JSON_TYPE_STRING, id_printable, sizeof(id_printable));

    // kbuckets
    struct json *kbuckets_obj = json_new(JSON_TYPE_ARRAY);

    for(int i=0; i < DHT_ROUTING_TABLE_SIZE; ++i)
    {
        if(rt->kbuckets[i]->size != 0) // ignore empty kbuckets
            json_add(kbuckets_obj, dht_kbucket_dump(rt->kbuckets[i]));
    }

    json_new_set_add(root_obj, JSON_TYPE_STRING, "kbuckets", 8);
    json_add(root_obj, kbuckets_obj);

    return root_obj;
}
