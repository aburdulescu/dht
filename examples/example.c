#include <dht.h>

int main(int argc, char *argv[])
{
    // declare and create dht structure
    dht_t *dht;
    dht_new(dht);

    // bootstrap or not
    int bootstrap_needed = 0;
    struct NetworkAddress bootstrap_addr;
    memset(&bootstrap_addr, 0, sizeof(struct NetworkAddress));
    if(argc == 3)
    {
        bootstrap_needed = 1;
        // trust user input
        memcpy(bootstrap_addr.ip, argv[1], strlen(argv[1]));
        bootstrap_addr.port = atoi(argv[2]);
    }

    if(bootstrap_needed)
    {
        printf("bootstraping with node at %s:%d\n", bootstrap_addr.ip, bootstrap_addr.port);

        int rc = dht_bootstrap(&dht, &bootstrap_addr);
        if(rc == -1)
            return 1;
    }
    else
    {
        printf("Running as bootstrap node on [%s:%d]\n", dht.addr.ip, dht.addr.port);
    }


    // now search for a key(id of a peer or id of a file)

    // id - the key to search for
    NodeId id;

    // res - a linked list that:
    //   - is empty, if the key is not on the dht
    //   - contains one node, if the provided key is the id of a peer
    //   - contains one or more nodes, if the provided key is the id of a file
    struct DhtNode *res;

    // blocking call
    dht_find(dht, id, res);

    // use the result

    // free memory
    dht_node_delete(res);

    // free dht resource
    dht_delete(dht);

    return 0;
}
