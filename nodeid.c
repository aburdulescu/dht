#include "nodeid.h"

#include <string.h>

#include "hash.h"


static int convert_pchar(char pchar);

int dht_nodeid_new(dht_nodeid_t nid, const unsigned char *data, const int data_sz)
{
    int rc = 0;
    unsigned char *hash = NULL;
    int hash_len = 0;

    if(dht_hash_sha1(data, data_sz, &hash, &hash_len) == -1)
    {
        printf("error: hash_generate()\n");
        rc = -1;
        goto cleanup;
    }
    if(hash_len != DHT_NODEID_LENGTH)
    {
        printf("error: %s: hash_len != DHT_NODEID_LENGTH\n", __FUNCTION__);
        rc = -1;
        goto cleanup;
    }
    memcpy(nid, hash, hash_len);

cleanup:
    free(hash);

    return rc;
}

void dht_nodeid_btop(const dht_nodeid_t nid, dht_nodeid_printable_t out)
{
    memset(out, 0, DHT_NODEID_PRINTABLE_LENGTH);
    char tmp[3];
    for(int i = 0; i < DHT_NODEID_LENGTH; ++i)
    {
        memset(tmp, 0, 3);
        sprintf(tmp, "%02x", nid[i]);
        out[2*i] = tmp[0];
        out[2*i+1] = tmp[1];
    }
}

int dht_nodeid_ptob(const char *pnid, const int pnid_len, dht_nodeid_t nid)
{
    if(pnid_len != (2 * DHT_NODEID_LENGTH))
        return -1;
    for(int i = 0; i < DHT_NODEID_LENGTH; ++i)
    {
        nid[i] = (convert_pchar(pnid[2*i]) << 4) | convert_pchar(pnid[2*i+1]);
    }
    return 0;
}

// convert hex char to binary char
static int convert_pchar(char pchar)
{
    unsigned char ret = 0;
    switch(pchar)
    {
    case '0': ret = 0; break;
    case '1': ret = 1; break;
    case '2': ret = 2; break;
    case '3': ret = 3; break;
    case '4': ret = 4; break;
    case '5': ret = 5; break;
    case '6': ret = 6; break;
    case '7': ret = 7; break;
    case '8': ret = 8; break;
    case '9': ret = 9; break;
    case 'a': ret = 10; break;
    case 'b': ret = 11; break;
    case 'c': ret = 12; break;
    case 'd': ret = 13; break;
    case 'e': ret = 14; break;
    case 'f': ret = 15; break;
    default:
        break;
    };
    return ret;
}

int dht_nodeid_prefixLength(const dht_nodeid_t nid)
{
    for(int i = 0; i < DHT_NODEID_LENGTH; ++i)
        for(unsigned char j = 0; j < 8; j++)
            if((nid[i]>>(unsigned char)(7 - j) & 0x01) == 0x01)
                return i * 8 + j;
    return DHT_NODEID_LENGTH * 8 - 1;
}

void dht_nodeid_xor(const dht_nodeid_t nid0, const dht_nodeid_t nid1, dht_nodeid_t res_nid)
{
    for(int i = 0; i < DHT_NODEID_LENGTH; ++i)
    {
        res_nid[i] = nid0[i] ^ nid1[i];
    }
}

int dht_nodeid_equals(const dht_nodeid_t nid0, const dht_nodeid_t nid1)
{
    for(int i = 0; i < DHT_NODEID_LENGTH; ++i)
        if(nid0[i] != nid1[i])
            return 0;
    return 1;
}

int dht_nodeid_compare(const dht_nodeid_t nid0, const dht_nodeid_t nid1)
{
    int i = 0;

    for(; i < DHT_NODEID_LENGTH && nid0[i] == nid1[i]; ++i);

    if(i == DHT_NODEID_LENGTH)
        return 0;
    else if(nid0[i] > nid1[i])
        return -1;
    else
        return 1;
}

void dht_nodeid_copy(dht_nodeid_t dest, const dht_nodeid_t src)
{
    memcpy(dest, src, DHT_NODEID_LENGTH);
}
