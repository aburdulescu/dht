#ifndef __DHT_STACK__
#define __DHT_STACK__

/**
 * @file stack.h
 */

#include <stdio.h>

struct stack
{
    struct stack* next;
    void* data;
};

struct stack* stack_new();
void stack_delete(struct stack *st);
void stack_push(struct stack **st, void* data);
void* stack_pop(struct stack **st);
void* stack_peek(struct stack *st);
int stack_is_empty(struct stack *st);

#endif
