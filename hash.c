#include "hash.h"

#include <string.h>


int hash_new(const char *digest_name,
             const unsigned char *data,
             const int data_sz,
             unsigned char **hash,
             int *hash_len);


int dht_hash_sha1(const unsigned char *data,
                  const int data_sz,
                  unsigned char **hash,
                  int *hash_len)
{
    return hash_new("sha1", data, data_sz, hash, hash_len);
}

int dht_hash_compare(dht_hash_t *h1, dht_hash_t *h2)
{
    while(*h1 && (*h1 == *h2))
    {
        h1++;
        h2++;
    }
    return *(const unsigned char*)h1 - *(const unsigned char*)h2;
}

void dht_hash_sha1_btop(const dht_hash_t *h, char *out)
{
    memset(out, 0, DHT_HASH_SHA1_PRINTABLE_LENGTH);
    char tmp[3];
    for(int i = 0; i < DHT_HASH_SHA1_LENGTH; ++i)
    {
        memset(tmp, 0, 3);
        sprintf(tmp, "%02x", h[i]);
        out[2*i] = tmp[0];
        out[2*i+1] = tmp[1];
    }
}

int hash_new(const char *digest_name,
             const unsigned char *data,
             const int data_sz,
             unsigned char **hash,
             int *hash_len)
{
    if(digest_name == NULL)
    {
        printf("error: %s: digest_name == NULL\n", __FUNCTION__);
        return -1;
    }
    if(data == NULL)
    {
        printf("error: %s: data == NULL\n", __FUNCTION__);
        return -1;
    }

    int           rc    = 0;
    const EVP_MD *md    = NULL;
    EVP_MD_CTX   *mdctx = NULL;

    md = EVP_get_digestbyname(digest_name);
    if(md == NULL)
    {
        printf("error: unknown message digest %s\n", digest_name);
        rc = -1;
        goto cleanup;
    }

    mdctx = EVP_MD_CTX_new();

    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, data, data_sz);

    unsigned char md_value[EVP_MAX_MD_SIZE];
    unsigned int md_len;
    EVP_DigestFinal_ex(mdctx, md_value, &md_len);

    *hash = (unsigned char *)malloc(md_len);
    memcpy(*hash, md_value, md_len);

    *hash_len = md_len;

cleanup:
    EVP_MD_CTX_free(mdctx);

    return rc;
}
