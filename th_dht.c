#include "th_dht.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>

#include "dht.h"
#include "log.h"


static void* run(void *args);


void dht_thread_rpc_start(struct dht_thread_rpc *t, void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    dht_thread_init(t, run, args);
}

void dht_th_stop(struct dht_thread *t)
{

}

void dht_thread_rpc_deinit(struct dht_thread_rpc *t)
{
    dht_thread_deinit(t);
}

static void* run(void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht *dht = (struct dht *)args;

    int exit_needed = 0;
    while(!exit_needed)
    {
    }

    pthread_exit(NULL);
}
