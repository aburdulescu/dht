#ifndef __DHT_CONTACT__
#define __DHT_CONTACT__

#include "json.h"

/**
 * @file contact.h
 */

#include "netaddr.h"
#include "nodeid.h"

struct dht_contact
{
    dht_nodeid_t id;
    struct dht_netaddr addr;
};

int dht_contact_is_empty(const struct dht_contact *c);

/**
 * Dump contact to JSON object
 */
struct json* dht_contact_dump(const struct dht_contact *c);

#endif
