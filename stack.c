#include "stack.h"

#include <stdlib.h>

static struct stack* new_node(void *data, struct stack *next);

struct stack* stack_new()
{
    return new_node(0, 0);
}

void stack_delete(struct stack *st)
{
    struct stack *i = st;
    while(i != NULL)
    {
        struct stack *tmp = i;
        i = i->next;
        free(tmp);
    }
}

void stack_push(struct stack **st, void* data)
{
    struct stack *tmp = new_node(data, *st);
    *st = tmp;
}

void* stack_pop(struct stack **st)
{
    if(stack_is_empty(*st))
        return 0;
    struct stack *tmp = *st;
    *st = (*st)->next;
    void* popped = tmp->data;
    free(tmp);

    return popped;
}

void* stack_peek(struct stack *st)
{
    if(stack_is_empty(st))
        return 0;
    return st->data;
}

int stack_is_empty(struct stack *st)
{
    return !st->next;
}

struct stack* new_node(void *data, struct stack *next)
{
    struct stack *tmp;
    tmp = (struct stack*)malloc(sizeof(struct stack));
    if(!tmp)
        exit(0);
    tmp->data = data;
    tmp->next = next;
    return tmp;
}
