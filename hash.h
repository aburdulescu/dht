#ifndef __DHT_HASH__
#define __DHT_HASH__

#include <openssl/evp.h>

#define DHT_HASH_SHA1_LENGTH 20
#define DHT_HASH_SHA1_PRINTABLE_LENGTH (2*DHT_HASH_SHA1_LENGTH+1)

typedef unsigned char dht_hash_t;


int dht_hash_sha1(const unsigned char *data,
                  const int data_sz,
                  dht_hash_t **hash,
                  int *hash_len);

int dht_hash_compare(dht_hash_t *h0, dht_hash_t *h1);
void dht_hash_sha1_btop(const dht_hash_t *h, char *out);

#endif
