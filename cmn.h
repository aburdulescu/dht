#ifndef __DHT_CMN__
#define __DHT_CMN__

/**
 * @file cmn.h
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>


#ifdef __cplusplus
extern "C"{
#endif

#define DHT_K_VALUE 20
#define DHT_ALPHA_VALUE 3
#define DHT_ROUTING_TABLE_SIZE (DHT_NODEID_LENGTH * 8)


#define LOG_SYSCALL_ERROR(syscall)                              \
    printf("[.]--syscall error in %s():%s(): errno = %d, %s\n", \
           __FUNCTION__, syscall, errno, strerror(errno))


#ifdef __cplusplus
}
#endif

#endif
