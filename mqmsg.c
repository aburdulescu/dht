#include "mqmsg.h"

#include <string.h>
#include <stdlib.h>


struct dht_mqmsg* dht_mqmsg_new(enum dht_mqmsg_type type, void *data)
{
    struct dht_mqmsg *m = (struct dht_mqmsg *)malloc(sizeof(struct dht_mqmsg));
    if(m == NULL)
        return NULL;

    m->type = type;
    m->data = data;

    return m;
}

void dht_mqmsg_delete(struct dht_mqmsg *m)
{
    free(m);
}
