#include "kbucket.h"

#include <stdlib.h>
#include <string.h>


struct dht_kbucket* dht_kbucket_new()
{
    struct dht_kbucket *kb = (struct dht_kbucket *)malloc(sizeof(struct dht_kbucket));
    if(kb == NULL)
        return NULL;

    kb->nodes = NULL;
    kb->size = 0;

    return kb;
}

struct dht_kbucket_node* dht_kbucket_find(const struct dht_kbucket *kb, const dht_nodeid_t id)
{
    for(struct dht_kbucket_node *i = kb->nodes; i != NULL; i = i->next)
    {
        if(dht_nodeid_equals(i->contact.id, id))
            return i;
    }
    return NULL;
}

struct dht_kbucket_node* dht_kbucket_node_new(const dht_nodeid_t id, const struct dht_netaddr *addr)
{
    struct dht_kbucket_node *n = (struct dht_kbucket_node *)malloc(sizeof(struct dht_kbucket_node));
    if(n == NULL)
        return NULL;
    memset(n, 0, sizeof(struct dht_kbucket_node));
    if(id != NULL)
        dht_nodeid_copy(n->contact.id, id);
    if(addr != NULL)
        memcpy(&n->contact.addr, addr, sizeof(struct dht_netaddr));
    n->next = NULL;

    return n;
}

struct json* dht_kbucket_dump(const struct dht_kbucket *kb)
{
    // just dump the contact from each node into a array,
    // ignore the rest since is just for internal use

    struct json *root_obj = json_new(JSON_TYPE_ARRAY);

    for(struct dht_kbucket_node *i = kb->nodes; i != NULL; i = i->next)
    {
        json_add(root_obj, dht_contact_dump(&i->contact));
    }

    return root_obj;
}
