#include "mq.h"

#include "log.h"


void mq_init(struct mq *mq)
{
    pthread_mutexattr_t q_mtx_attr;
    pthread_mutexattr_init(&q_mtx_attr);
    pthread_mutexattr_settype(&q_mtx_attr, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&mq->q_mtx, &q_mtx_attr);
    pthread_mutexattr_destroy(&q_mtx_attr);

    pthread_mutexattr_t cv_mtx_attr;
    pthread_mutexattr_init(&cv_mtx_attr);
    pthread_mutexattr_settype(&cv_mtx_attr, PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_init(&mq->cv_mtx, &cv_mtx_attr);
    pthread_mutexattr_destroy(&cv_mtx_attr);

    pthread_cond_init(&mq->cv, NULL);
}

void mq_delete(struct mq *mq)
{
    DHT_LOG(LOG_DEBUG, "in");
    pthread_mutex_destroy(&mq->q_mtx);
    pthread_mutex_destroy(&mq->cv_mtx);
    pthread_cond_destroy(&mq->cv);

    queue_delete(mq->q);
}

void mq_push(struct mq *mq, void *data)
{
    pthread_mutex_lock(&mq->q_mtx);
    queue_push(&mq->q, data);
    pthread_mutex_unlock(&mq->q_mtx);
    pthread_cond_signal(&mq->cv);
}

int mq_is_empty(struct mq *mq)
{
    pthread_mutex_lock(&mq->q_mtx);
    int isEmpty = queue_is_empty(mq->q);
    pthread_mutex_unlock(&mq->q_mtx);

    return isEmpty;
}

void* mq_pop(struct mq *mq)
{
    void *d = NULL;
    int isEmpty = mq_is_empty(mq);

    if(!isEmpty)
    {
        pthread_mutex_lock(&mq->q_mtx);
        d = queue_pop(&mq->q);
        pthread_mutex_unlock(&mq->q_mtx);
    }

    return d;
}

void* mq_peek(struct mq *mq)
{
    void *d = NULL;
    int isEmpty = mq_is_empty(mq);

    if(!isEmpty)
    {
        pthread_mutex_lock(&mq->q_mtx);
        d = queue_peek(mq->q);
        pthread_mutex_unlock(&mq->q_mtx);
    }

    return d;
}

void* mq_wait_and_pop(struct mq *mq)
{
    while(mq_is_empty(mq))
    {
        pthread_mutex_lock(&mq->cv_mtx);
        pthread_cond_wait(&mq->cv, &mq->cv_mtx);
    }

    void *d = NULL;

    pthread_mutex_lock(&mq->q_mtx);
    d = queue_pop(&mq->q);
    pthread_mutex_unlock(&mq->q_mtx);

    return d;
}

void* mq_wait_and_peek(struct mq *mq)
{
    while(mq_is_empty(mq))
    {
        pthread_mutex_lock(&mq->cv_mtx);
        pthread_cond_wait(&mq->cv, &mq->cv_mtx);
    }

    void *d = NULL;

    pthread_mutex_lock(&mq->q_mtx);
    d = queue_peek(mq->q);
    pthread_mutex_unlock(&mq->q_mtx);

    return d;
}
