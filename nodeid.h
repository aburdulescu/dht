#ifndef __DHT_NODEID__
#define __DHT_NODEID__

/**
 * @file nodeid.h
 */

#ifdef __cplusplus
extern "C"{
#endif

#define DHT_NODEID_LENGTH 20
#define DHT_HASH_ALGORITHM "sha1"
#define DHT_NODEID_PRINTABLE_LENGTH (2*DHT_NODEID_LENGTH+1)

/**
 * nodeid type
*/
typedef unsigned char dht_nodeid_t[DHT_NODEID_LENGTH];

/**
 * nodeid type used for printing
*/
typedef unsigned char dht_nodeid_printable_t[DHT_NODEID_PRINTABLE_LENGTH];

int   dht_nodeid_new(dht_nodeid_t nid, const unsigned char *data, const int data_sz);
void dht_nodeid_btop(const dht_nodeid_t nid, dht_nodeid_printable_t out);
int   dht_nodeid_ptob(const char *pnid, const int pnid_len, dht_nodeid_t nid);
int   dht_nodeid_prefixLength(const dht_nodeid_t nid);
void dht_nodeid_xor(const dht_nodeid_t nid0, const dht_nodeid_t nid1, dht_nodeid_t res_nid);
int   dht_nodeid_equals(const dht_nodeid_t nid0, const dht_nodeid_t nid1);
int   dht_nodeid_compare(const dht_nodeid_t nid0, const dht_nodeid_t nid1);
void dht_nodeid_copy(dht_nodeid_t dest, const dht_nodeid_t src);

#ifdef __cplusplus
}
#endif

#endif
