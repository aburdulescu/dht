#ifndef __DHT_RTABLE__
#define __DHT_RTABLE__

/**
 * @file rtable.h
 */

#include "nodeid.h"
#include "netaddr.h"
#include "json.h"
#include "clist.h"

#include "kbucket.h"

struct dht_rtable
{
    struct dht_kbucket* kbuckets[DHT_ROUTING_TABLE_SIZE];
    dht_nodeid_t id;
};

void dht_rtable_new(struct dht_rtable *rt, const dht_nodeid_t id);
void dht_rtable_find_k_closest(const struct dht_rtable *rt, const dht_nodeid_t id, dht_clist_t contacts);
void dht_rtable_print(const struct dht_rtable *rt);
void dht_rtable_delete(const struct dht_rtable *rt);
void dht_rtable_update(const struct dht_rtable *rt, const struct dht_contact *c);

/**
 * Dump routing table to JSON object
 */
struct json* dht_rtable_dump(const struct dht_rtable *rt);

#endif
