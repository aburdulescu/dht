#ifndef __DHT_TH_RTABLE__
#define __DHT_TH_RTABLE__

#include "dht_thread.h"


struct dht_thread_rtable
{
    struct dht_thread t;
};

void dht_thread_rtable_start(struct dht_thread_rtable *t, void *args);
void dht_thread_rtable_stop(struct dht_thread_rtable *t);
void dht_thread_rtable_deinit(struct dht_thread_rtable *t);

#endif
