#include "rspmap.h"

#include <string.h>

#include "hash.h"


static dht_rspmap_entry_t* entry_new(dht_hash_t *hash,
                                     dht_rspmap_cb_t cb,
                                     unsigned int timeout);


dht_rspmap_t* dht_rspmap_new()
{
    dht_rspmap_t *m = (dht_rspmap_t *)malloc(sizeof(dht_rspmap_t));
    if(m == NULL)
        return NULL;

    m->map = NULL;
    m->len = 0;

    return m;
}

void dht_rspmap_delete(dht_rspmap_t *m)
{
    free(m);
}

void dht_rspmap_insert(dht_rspmap_t **m,
                       dht_hash_t *hash,
                       dht_rspmap_cb_t cb,
                       unsigned int timeout)
{
    dht_rspmap_entry_t *e = entry_new(hash, cb, timeout);

    e->next = (*m)->map;
    (*m)->map = e;
}

dht_rspmap_entry_t* dht_rspmap_get(dht_rspmap_t *m, dht_hash_t *h)
{
    dht_rspmap_entry_t *i;
    for( i = m->map; i != NULL; i = i->next)
    {
        if(dht_hash_compare(h, i->hash) == 0)
            break;
    }
    return i;
}

void dht_rspmap_print(dht_rspmap_t *m)
{
    int c = 0;
    for(dht_rspmap_entry_t *i = m->map; i != NULL; i = i->next, c++)
    {
        printf("entry %d:\n", c);
        char hash_printable[DHT_HASH_SHA1_PRINTABLE_LENGTH];
        dht_hash_sha1_btop(i->hash, hash_printable);
        printf("\thash: %s\n", i->hash);
        printf("\ttimestamp: %ld\n", i->timestamp);
        printf("\ttimeout: %d\n", i->timeout);
    }
}

static dht_rspmap_entry_t* entry_new(dht_hash_t *hash,
                                     dht_rspmap_cb_t cb,
                                     unsigned int timeout)
{
    dht_rspmap_entry_t *e = (dht_rspmap_entry_t *)malloc(sizeof(dht_rspmap_entry_t));
    if(e == NULL)
        return NULL;

    e->next = NULL;
    e->hash = (dht_hash_t *)strdup((const char *)hash);
    e->cb = cb;
    e->timeout = timeout;
    e->timestamp = (unsigned long)time(NULL);

    return e;
}
