#ifndef __DHT_TH_RPC__
#define __DHT_TH_RPC__

#include "dht_thread.h"


struct dht_thread_rpc
{
    int pipefd;
    struct dht_thread t;
};

void dht_thread_rpc_start(struct dht_thread_rpc *t, void *args);
void dht_thread_rpc_stop(struct dht_thread_rpc *t);
void dht_thread_rpc_mqwakeup(struct dht_thread_rpc *t);
void dht_thread_rpc_deinit(struct dht_thread_rpc *t);

#endif
