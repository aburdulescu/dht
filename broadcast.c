#include "broadcast.h"

#include <ifaddrs.h>
#include <stdio.h>
#include <string.h>


int ip4addr_btop(const struct sockaddr *addr, char *ip);


int get_broadcast_addr(char *ip, struct sockaddr *addr)
{
    struct ifaddrs *ifaddr;
    if (getifaddrs(&ifaddr) == -1) {
        perror("getifaddrs");
        return -1;
    }

    int rc = -1;
    for(struct ifaddrs *ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;
        if(strcmp(ifa->ifa_name, "lo") == 0)
            continue;

        if(ifa->ifa_addr->sa_family == AF_INET)
        {
            rc = ip4addr_btop(ifa->ifa_broadaddr, ip);
            if(rc < 0)
            {
                freeifaddrs(ifaddr);
                return -1;
            }
            memcpy(addr, ifa->ifa_broadaddr, sizeof(struct sockaddr));
            break;
        }
    }

    freeifaddrs(ifaddr);

    return rc;
}

int ip4addr_btop(const struct sockaddr *addr, char *ip)
{
    int rc = getnameinfo(addr,
                         sizeof(struct sockaddr_in),
                         ip,
                         NI_MAXHOST,
                         NULL,
                         0,
                         NI_NUMERICHOST);
    if (rc != 0)
    {
        printf("getnameinfo() failed: %s\n", gai_strerror(rc));
        return -1;
    }

    return 0;
}
