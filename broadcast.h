#ifndef __DHT_BROADCAST__
#define __DHT_BROADCAST__

#include <arpa/inet.h>
#include <netdb.h>


#define BROADCAST_IP_STRLEN NI_MAXHOST

int get_broadcast_addr(char *ip, struct sockaddr *addr);

#endif
