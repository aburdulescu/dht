#include "contact.h"

int dht_contact_is_empty(const struct dht_contact *c)
{
    int ret = 0;
    dht_nodeid_t empty_id = {0};
    ret |= (dht_nodeid_equals(c->id, empty_id)) ? 1 : 0;
//    ret |= (c->addr.ip == NULL) ? 1 : 0; // TODO: fix it
    ret |= (c->addr.port == 0) ? 1 : 0;

    return ret;
}

struct json* dht_contact_dump(const struct dht_contact *c)
{
    struct json *root_obj = json_new(JSON_TYPE_OBJECT);

    // id
    dht_nodeid_printable_t id_printable;
    dht_nodeid_btop(c->id, id_printable);
    json_new_set_add(root_obj, JSON_TYPE_STRING, "id", 2);
    json_new_set_add(root_obj, JSON_TYPE_STRING, id_printable, sizeof(id_printable));

    // addr
    json_new_set_add(root_obj, JSON_TYPE_STRING, "addr", 4);
    json_add(root_obj, dht_netaddr_dump(&c->addr));

    return root_obj;

}
