#ifndef __DHT_TH_DHT__
#define __DHT_TH_DHT__

#include "dht_thread.h"


void dht_th_start(struct dht_thread *t, void *args);
void dht_th_stop(struct dht_thread *t);
void dht_th_deinit(struct dht_thread *t);

#endif
