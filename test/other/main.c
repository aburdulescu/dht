#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mycdht/rpc.h"
#include "tinytest.h"
#include "tinytest_macros.h"

static void test_dht_rpc_message_pack_unpack(void *dummy_arg)
{
    (void) dummy_arg; /* we're not using this argument. */

    char str0[] = "pack/unpack example";
    dht_nodeid_t sender;
    dht_nodeid_new(sender, (unsigned char *)str0, strlen(str0));

    char str1[] = "some_key";
    dht_nodeid_t search_key;
    dht_nodeid_new(search_key, (unsigned char *)str1, strlen(str1));

    struct dht_rpc_message m_in;
    memset(&m_in, 0, sizeof(m_in));
    m_in.type = DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ;
    dht_nodeid_copy(m_in.sender, sender);
    dht_nodeid_copy(m_in.data.id, search_key);

    printf("Message before pack:\n");
    struct json *jo_in = dht_rpc_message_dump(&m_in);
    char *str_in = json_dump(jo_in, 2);
    printf("%s", str_in);
    free(str_in);
    json_delete(jo_in);

    struct dht_binary_data d;
    memset(&d, 0, sizeof(d));
    dht_rpc_message_pack(&m_in, &d);

    struct dht_rpc_message m_out;
    dht_rpc_message_unpack(&d, &m_out);

    printf("Message after unpack:\n");
    struct json *jo_out = dht_rpc_message_dump(&m_out);
    char *str_out = json_dump(jo_out, 2);
    printf("%s", str_out);
    free(str_out);
    json_delete(jo_out);

    tt_assert_msg(m_in.type == m_out.type, "The two messages have different senders");
    tt_assert_msg(dht_nodeid_compare(m_in.sender, m_out.sender) == 0, "The two messages have different senders");

  end:
    ;
}

struct testcase_t rpc_tests[] = {
    { "dht_rpc_message_pack_unpack", test_dht_rpc_message_pack_unpack, 0, NULL, NULL },
    END_OF_TESTCASES
};

struct testgroup_t test_groups[] = {
    { "rpc/", rpc_tests },
    END_OF_GROUPS
};

int main(int argc, const char **argv)
{
    return tinytest_main(argc, argv, test_groups);
}
