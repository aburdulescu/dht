#include "json.h"

#include <string.h>
#include <stdlib.h>

int main()
{
    struct json *root_obj = json_new(JSON_TYPE_OBJECT);

    char sender[] = "sender";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, sender, strlen(sender)) != JSON_ERROR_OK)
        return 1;

    char sender_value[] = "sender_value";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, sender_value, strlen(sender_value)) != JSON_ERROR_OK)
        return 1;

    char array[] = "array";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, array, strlen(array)) != JSON_ERROR_OK)
        return 1;

    struct json *array_obj = json_new(JSON_TYPE_ARRAY);

    char type1[] = "type1";
    if(json_new_set_add(array_obj, JSON_TYPE_STRING, type1, strlen(type1)) != JSON_ERROR_OK)
        return 1;

    int type1_value = 1;
    if(json_new_set_add(array_obj, JSON_TYPE_NUMBER, &type1_value, sizeof(type1_value)) != JSON_ERROR_OK)
        return 1;

    if(json_add(root_obj, array_obj) != JSON_ERROR_OK)
        return 1;

    char type2[] = "type2";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, type2, strlen(type2)) != JSON_ERROR_OK)
        return 1;

    int type2_value = 2;
    if(json_new_set_add(root_obj, JSON_TYPE_NUMBER, &type2_value, sizeof(type2_value)) != JSON_ERROR_OK)
        return 1;

    char type3[] = "type3";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, type3, strlen(type3)) != JSON_ERROR_OK)
        return 1;

    int type3_value = 3;
    if(json_new_set_add(root_obj, JSON_TYPE_NUMBER, &type3_value, sizeof(type3_value)) != JSON_ERROR_OK)
        return 1;

    char array1[] = "array1";
    if(json_new_set_add(root_obj, JSON_TYPE_STRING, array1, strlen(array1)) != JSON_ERROR_OK)
        return 1;

    struct json *array1_obj = json_new(JSON_TYPE_ARRAY);

    char type4[] = "type4";
    if(json_new_set_add(array1_obj, JSON_TYPE_STRING, type4, strlen(type4)) != JSON_ERROR_OK)
        return 1;

    int type4_value = 4;
    if(json_new_set_add(array1_obj, JSON_TYPE_NUMBER, &type4_value, sizeof(type4_value)) != JSON_ERROR_OK)
        return 1;

    if(json_add(root_obj, array1_obj) != JSON_ERROR_OK)
        return 1;

    char *json_string = json_dump(root_obj, 2);

    printf("%s", json_string);

    FILE *out_s = fopen("json_test.json", "w");
    fprintf(out_s, "%s", json_string);
    fclose(out_s);

    free(json_string);

    json_delete(root_obj);

    return 0;
}
