#include "../../broadcast.h"

#include <stdio.h>
#include <string.h>


int main()
{
    char broadcast_ip[BROADCAST_IP_STRLEN];
    memset(broadcast_ip, 0, BROADCAST_IP_STRLEN);

    struct sockaddr broadcast_addr;

    if(get_broadcast_addr(broadcast_ip, &broadcast_addr) < 0)
    {
        return 1;
    }

    printf("Broadcast ip: %s\n", broadcast_ip);

    return 0;
}
