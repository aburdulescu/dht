#include <stdio.h>

#include "../../rspmap.h"
#include "../../hash.h"


void map_cb(void *res, dht_rspmap_status_t status);


int main()
{
    dht_rspmap_t *m = dht_rspmap_new();

    for(int i = 0; i < 10; i++)
    {
        dht_hash_t *h;
        int h_len;
        dht_hash_sha1((dht_hash_t*)"elem0", 5, &h, &h_len);

        dht_rspmap_insert(&m, h, map_cb, 2);
    }

    dht_rspmap_print(m);

    dht_rspmap_delete(m);

    return 0;
}

void map_cb(void *res, dht_rspmap_status_t status)
{

}
