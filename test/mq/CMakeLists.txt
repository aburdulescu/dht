cmake_minimum_required(VERSION 3.7)

project(mq)

set(CMAKE_BUILD_TYPE RelWithDebInfo)

set(INC_PATH
  ../../
  )
set(SOURCES
  main.c
  ../../mq.c
  ../../queue.c
  )

set(HEADERS
  ../../mq.h
  ../../queue.h
  )

set(LIBS
  pthread
  )

add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})
target_include_directories(${PROJECT_NAME} PRIVATE ${INC_PATH})
target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra)
target_link_libraries(${PROJECT_NAME} ${LIBS})
