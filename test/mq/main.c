#include "mq.h"

#include <stdio.h>
#include <stdlib.h>


static void* th_func(void *args);

struct mq mq;


int main()
{
    mq_init(&mq);

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    pthread_t th;
    pthread_create(&th, &attr, th_func, NULL);

    int* numbers[10];
    for(int i = 0; i < 10; i++)
    {
        numbers[i] = (int *)malloc(sizeof(int));
        *numbers[i] = i;

        mq_push(&mq, numbers[i]);
        printf("-> %d\n", *numbers[i]);

        int *p = mq_wait(&mq);
        printf("<- %d\n", *p);
    }

    for(int i = 0; i < 10; i++)
    {
        free(numbers[i]);
    }

    mq_delete(&mq);

    return 0;
}

static void* th_func(void *args)
{
    for(int c = 10; c; c--)
    {
        int *p = mq_wait(&mq);
        mq_push(&mq, p);
    }

    pthread_exit(NULL);
}
