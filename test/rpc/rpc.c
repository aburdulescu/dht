#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "mycdht/rpc.h"


int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "error: request type not specified(ping or find_node)\n");
        return 1;
    }

    char str0[] = "rpc_req";
    dht_nodeid_t sender;
    dht_nodeid_new(sender, (unsigned char *)str0, strlen(str0));

    struct dht_rpc r;
    int rc = dht_rpc_open(&r, 0);
    assert(rc == 0);

    struct dht_netaddr server_addr = {
        .ip =  "127.0.0.1",
        .port = 2323
    };

    if(strcmp(argv[1], "ping") == 0)
    {
        fprintf(stderr, "PING sent\n");
        rc = dht_rpc_ping(&r, sender, &server_addr);
        fprintf(stderr, "PING ok? %s\n", (rc == 0) ? "yes" : "no");
    }
    else if(strcmp(argv[1], "find_node") == 0)
    {
        char str1[] = "some_key";
        dht_nodeid_t search_key;
        dht_nodeid_new(search_key, (unsigned char *)str1, strlen(str1));

        dht_clist_t contacts;

        fprintf(stderr, "FIND_NODE sent\n");
        rc = dht_rpc_find_node(&r, sender, &server_addr, search_key, contacts);
        if(rc == -1)
        {
            fprintf(stderr, "FIND_NODE failed\n");
            return 1;
        }

        struct json *contacts_obj = json_new(JSON_TYPE_OBJECT);
        json_new_set_add(contacts_obj, JSON_TYPE_STRING, "data", 4);
        json_add(contacts_obj, dht_clist_dump(contacts));
        char *contacts_str = json_dump(contacts_obj, 2);

        fprintf(stderr, "FIND_NODE result:\n%s", contacts_str);

        free(contacts_str);
        json_delete(contacts_obj);
    }
    else
    {
        fprintf(stderr, "error: unknown request type '%s'\n", argv[1]);
        return 1;
    }

    return 0;
}
