#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "mycdht/rpc.h"


void create_clist(dht_clist_t l);


int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "error: endpoint type not specified('req' or 'rep')\n");
    }

    char str0[] = "read/write example";
    dht_nodeid_t sender;
    dht_nodeid_new(sender, (unsigned char *)str0, strlen(str0));

    if(strcmp(argv[1], "req") == 0)
    {
        struct dht_rpc r;
        int rc = dht_rpc_open(&r, 0);
        assert(rc == 0);

        fprintf(stderr, "RPC endpoint: %s:%d\n", r.addr.ip, r.addr.port);

        struct dht_netaddr server_addr = {
            .ip =  "127.0.0.1",
            .port = 2323
        };

        fprintf(stderr, "PING sent\n");
        rc = dht_rpc_ping(&r, sender, &server_addr);
        fprintf(stderr, "PING ok? %s\n", (rc == 0) ? "yes" : "no");

        char str1[] = "some_key";
        dht_nodeid_t search_key;
        dht_nodeid_new(search_key, (unsigned char *)str1, strlen(str1));

        dht_clist_t contacts;

        fprintf(stderr, "FIND_NODE sent\n");
        rc = dht_rpc_find_node(&r, sender, &server_addr, search_key, contacts);
        if(rc == -1)
        {
            fprintf(stderr, "FIND_NODE failed\n");
            return 1;
        }

        struct json *contacts_obj = json_new(JSON_TYPE_OBJECT);
        json_new_set_add(contacts_obj, JSON_TYPE_STRING, "data", 4);
        json_add(contacts_obj, dht_clist_dump(contacts));
        char *contacts_str = json_dump(contacts_obj, 2);

        fprintf(stderr, "FIND_NODE result:\n%s", contacts_str);

        free(contacts_str);
        json_delete(contacts_obj);
    }
    else if(strcmp(argv[1], "rep") == 0)
    {
        struct dht_rpc r;
        int rc = dht_rpc_open(&r, 2323);
        assert(rc == 0);

        fprintf(stderr, "RPC endpoint: %s:%d\n", r.addr.ip, r.addr.port);

        while(1)
        {
            struct dht_rpc_message m_in;
            struct dht_netaddr addr;
            int rc = dht_rpc_read(&r, &m_in, &addr);
            assert(rc == 0);

            switch(m_in.type) {
            case DHT_RPC_MESSAGE_TYPE_PING_REQ:
            {
                rc = dht_rpc_ping_rsp(&r, sender, &addr);
                assert(rc == 0);
                break;
            }
            case DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ:
            {
                dht_clist_t contacts;
                create_clist(contacts);
                rc = dht_rpc_find_node_rsp(&r, sender, &addr, contacts);
                assert(rc == 0);
                break;
            }
            default:
                break;
            }
        }
    }

    return 0;
}

void create_clist(dht_clist_t l)
{
    char id_str_base[] = "distance";
    char id_str[strlen(id_str_base)+3];

    char tmp[3];

    for(int i = 0; i < DHT_K_VALUE; ++i)
    {
        struct dht_clist_elem cr;

        memset(id_str, 0, strlen(id_str));
        strcpy(id_str, id_str_base);

        memset(tmp, 0, 3);
        sprintf(tmp, "%d", i);

        strcat(id_str, tmp);

        dht_nodeid_t id;
        dht_nodeid_new(id, (unsigned char *)id_str, strlen(id_str));

        dht_nodeid_copy(cr.distance, id);
        dht_nodeid_copy(cr.contact.id, id);
        strcpy(cr.contact.addr.ip, "127.0.0.1");
        cr.contact.addr.port = 2323;

        memcpy(&l[i], &cr, sizeof(cr));
    }
}
