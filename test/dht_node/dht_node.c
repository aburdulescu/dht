#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "mycdht/dht.h"
#include "mycdht/log.h"

void  catch_sigint();
void  sigint_handler(int signum, siginfo_t *info, void *ptr);


struct dht dht;


int main(int argc, char *argv[])
{
    catch_sigint();

    // start dht logging
    dht_log_open("dht_node");

    // a string that will be hashed and used as a the node id
    char id_str[] = "dht_node";

    // create dht structure
    dht_new(&dht, id_str, 2323);

    // start dht
    dht_start(&dht);

    /*
    // bootstrap into the network
    struct dht_netaddr bootstrap_addr = {ip="some ip", port=2323};
    if(dht_bootstrap(&dht, &bootstrap_addr) < 0)
    {
        printf("error: bootstrap with node %s:%d failed!\n", bootstrap_addr.ip, bootstrap_addr.port);
        return 1;
    }

    // search for another node
    struct dht_contact c;
    dht_nodeid_t wanted_id;
    if(dht_find_node(&dht, wanted_id, &c) < 0)
    {
        printf("error: find_node %s failed!\n", wanted_id);
        return 1;
    }
    */

    while(1);
}

void catch_sigint()
{
    static struct sigaction _sigact;

    memset(&_sigact, 0, sizeof(_sigact));
    _sigact.sa_sigaction = sigint_handler;
    _sigact.sa_flags = SA_SIGINFO;

    sigaction(SIGINT, &_sigact, NULL);
    sigaction(SIGTERM, &_sigact, NULL);
}

void sigint_handler(int signum, siginfo_t *info, void *ptr)
{
    printf("\nSignal %d caught, bye!\n", signum);

    dht_stop(&dht);
    dht_save(&dht, "rtable.json");
    dht_delete(&dht);

    dht_log_close();

    exit(0);
}
