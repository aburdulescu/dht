#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uv.h>
#include <netinet/in.h>


uv_loop_t *l;

void on_send(uv_udp_send_t *req, int status);
void on_recv(uv_udp_t *req, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned flags);
void on_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf);


int main()
{
    l = uv_default_loop();

    uv_udp_t s;
    uv_udp_init(l, &s);

    struct sockaddr_in recv_addr;
    uv_ip4_addr("0.0.0.0", 2323, &recv_addr);

    uv_udp_bind(&s, (const struct sockaddr*)&recv_addr, 0);

    printf("start reading ...\n");
    uv_udp_recv_start(&s, on_alloc, on_recv);

    printf("Waiting for clients ...\n");
    return uv_run(l, UV_RUN_DEFAULT);
}

void on_send(uv_udp_send_t *req, int status)
{
    if(status)
    {
        fprintf(stderr, "Send error %s\n", uv_strerror(status));
        return;
    }
    printf("send success\n");
}

void on_recv(
    uv_udp_t *req,
    ssize_t nread,
    const uv_buf_t *buf,
    const struct sockaddr *addr,
    unsigned flags)
{
    if(nread == 0)
    {
        if(addr == NULL)
            fprintf(stderr, "%s - end of datagram\n", __FUNCTION__);
        else
            fprintf(stderr, "%s - error: empty datagram\n", __FUNCTION__);
        goto cleanup;
    }

    if (nread < 0)
    {
        fprintf(stderr, "%s - error: %s\n", __FUNCTION__, uv_err_name(nread));
        goto cleanup;
    }

    char sender[17] = {0};
    uv_ip4_name((const struct sockaddr_in*) addr, sender, 16);
    fprintf(stderr, "Recv from %s\n", sender);

    char *msg = (char*)malloc(nread);
    memcpy(msg, buf->base, nread);
    msg[nread] = 0;
    fprintf(stderr, "%s\n", msg);

    uv_udp_send_t *send_req = (uv_udp_send_t*)malloc(sizeof(uv_udp_send_t));
    uv_buf_t buf_send = uv_buf_init(msg, nread);

    for(int i=0; i<nread; ++i)
    {
        buf_send.base[i]=msg[i];
    }
    buf_send.base[nread]=0;
    printf("sending back %s\n", buf_send.base);
//    free(msg);

    uv_udp_send(send_req, req, &buf_send, 1, (const struct sockaddr*)addr, on_send);

cleanup:
    free(buf->base);
}

void on_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf)
{
    buf->base = malloc(suggested_size);
    buf->len = suggested_size;
}
