#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <uv.h>
#include <netinet/in.h>


uv_loop_t *l;

void on_send(uv_udp_send_t *req, int status);
void on_recv(uv_udp_t *req, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned flags);
void on_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf);


int main()
{
    l = uv_default_loop();

    uv_udp_t s;
    uv_udp_init(l, &s);

    struct sockaddr_in recv_addr;
    uv_ip4_addr("0.0.0.0", 2324, &recv_addr);

    uv_udp_bind(&s, (const struct sockaddr*)&recv_addr, 0);

    uv_udp_recv_start(&s, on_alloc, on_recv);

    struct sockaddr_in serv_addr;
    uv_ip4_addr("127.0.0.1", 2323, &serv_addr);

    uv_udp_send_t *send_req = (uv_udp_send_t*)malloc(sizeof(uv_udp_send_t));
    uv_buf_t buf_send = uv_buf_init("hello", 5);
    uv_udp_send(send_req, &s, &buf_send, 1, (const struct sockaddr*)&serv_addr, on_send);

    uv_udp_recv_start(&s, on_alloc, on_recv);
    return uv_run(l, UV_RUN_DEFAULT);
}

void on_send(uv_udp_send_t *req, int status)
{
    if(status)
    {
        fprintf(stderr, "Send error %s\n", uv_strerror(status));
        return;
    }
    printf("send success\n");
}

void on_recv(
    uv_udp_t *req,
    ssize_t nread,
    const uv_buf_t *buf,
    const struct sockaddr *addr,
    unsigned flags)
{
    if(nread == 0)
    {
        if(addr == NULL)
            fprintf(stderr, "%s - end of datagram\n", __FUNCTION__);
        else
            fprintf(stderr, "%s - error: empty datagram\n", __FUNCTION__);
        uv_udp_recv_stop(req);
        uv_close((uv_handle_t*)req, NULL);
        goto cleanup;
    }

    if (nread < 0)
    {
        fprintf(stderr, "%s - error: %s\n", __FUNCTION__, uv_err_name(nread));
        uv_udp_recv_stop(req);
        uv_close((uv_handle_t*)req, NULL);
        goto cleanup;
    }

    char sender[17] = {0};
    uv_ip4_name((const struct sockaddr_in*) addr, sender, 16);
    fprintf(stderr, "Recv %s from %s\n", buf->base, sender);

cleanup:
    free(buf->base);
    uv_udp_recv_stop(req);
}

void on_alloc(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf)
{
    buf->base = malloc(suggested_size);
    buf->len = suggested_size;
}
