#ifndef __DHT_IP4ADDR__
#define __DHT_IP4ADDR__

/**
 * @file ip4addr.h
 */

#include <netinet/in.h>

#include "json.h"

#define DHT_IP4ADDR_LENGTH INET_ADDRSTRLEN

struct dht_ip4addr
{
    char ip[DHT_IP4ADDR_LENGTH];
    int port;
};

int dht_ip4addr_ntop(const struct sockaddr_in *n_addr, struct dht_ip4addr *p_addr);
int dht_ip4addr_pton(const struct dht_ip4addr *p_addr, struct sockaddr_in *n_addr);
int dht_ip4addr_equals(const struct dht_ip4addr *addr0, const struct dht_ip4addr *addr1);

/**
 * Dump ip4addr to JSON object
 */
struct json* dht_ip4addr_dump(const struct dht_ip4addr *addr);

#endif
