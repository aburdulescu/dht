#ifndef __DHT_RPC__
#define __DHT_RPC__

/**
 * @file rpc.h
 */

#include "rtable.h"
#include "netaddr.h"
#include "nodeid.h"
#include "json.h"

struct dht_binary_data
{
    void *value;
    size_t size;
};

enum dht_rpc_message_type
{
    DHT_RPC_MESSAGE_TYPE_PING_REQ,
    DHT_RPC_MESSAGE_TYPE_PING_RSP,
    DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ,
    DHT_RPC_MESSAGE_TYPE_FIND_NODE_RSP,
};

union dht_rpc_message_data
{
    dht_nodeid_t id; // FIND_NODE_REQ
    dht_clist_t contacts; // FIND_NODE_RSP
};

struct dht_rpc_message
{
    dht_nodeid_t sender;
    enum dht_rpc_message_type type;
    union dht_rpc_message_data data;
};

#define DHT_RPC_MESSAGE_MAX_LENGTH (sizeof(enum dht_rpc_message_type) + sizeof(dht_nodeid_t) + sizeof(union dht_rpc_message_data))

// UDP max payload is aprox 2^16
#define DHT_RPC_MESSAGE_DATA_SIZE 2

struct dht_rpc
{
    int fd;
    struct dht_netaddr addr;
};

int dht_rpc_open(struct dht_rpc *r, int port);
void dht_rpc_close(const struct dht_rpc *r);
int dht_rpc_ping(const struct dht_rpc *r, const dht_nodeid_t sender, const struct dht_netaddr *to);
int dht_rpc_find_node(const struct dht_rpc *r,
                      const dht_nodeid_t sender,
                      const struct dht_netaddr *to,
                      const dht_nodeid_t id,
                      dht_clist_t contacts);

// PRIVATE
int dht_rpc_ping_rsp(const struct dht_rpc *r, const dht_nodeid_t sender, const struct dht_netaddr *to);
int dht_rpc_find_node_rsp(const struct dht_rpc *r,
                          const dht_nodeid_t sender,
                          const struct dht_netaddr *to,
                          dht_clist_t contacts);

int dht_rpc_read(const struct dht_rpc *r, struct dht_rpc_message *m, struct dht_netaddr *from);
int dht_rpc_write(const struct dht_rpc *r, const struct dht_rpc_message *m, const struct dht_netaddr *to);
void dht_rpc_message_pack(const struct dht_rpc_message *m, struct dht_binary_data *d);
void dht_rpc_message_unpack(const struct dht_binary_data *d, struct dht_rpc_message *m);
struct json* dht_rpc_message_dump(const struct dht_rpc_message *m);

#endif
