#include "th_rtable.h"

#include "dht.h"
#include "mqmsg.h"
#include "log.h"


/*
 * TODO: merge rtable and rpc threads, no need for 2
 * use libuv
 * communication with remote peers via uv_udp_t and with main thread via uv_pipe_t
 * implement a map of pending responses:
 *  - the key is a hash of the response type concatenated with:
 *      - peer id for rpc responses;
 *      - own id for main thread responses(it will be only one pending response
 *        from the main thread since the api will be blocking)
 *  - the value is a callback which will be called when the response arrives
 *  - w.i.p. there must be a timeout mechanism to ensure that a response is not stuck forever(uv_timer)
 */

static void on_mqmsg_find_node(struct dht *dht, struct dht_mqmsg *m_in);
static void on_mqmsg_rtable_update(struct dht *dht, struct dht_mqmsg *m_in);
static void on_mqmsg_rtable_find_k_closest(struct dht *dht, struct dht_mqmsg *m_in);
static void on_mqmsg_stop(struct dht *dht, struct dht_mqmsg *m_in);

static void* run(void *args);

void dht_thread_rtable_start(struct dht_thread_rtable *t, void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    dht_thread_init(&t->t, run, args);
}

void dht_thread_rtable_stop(struct dht_thread_rtable *t)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_STOP, NULL);
    mq_push(&t->t.mq, m);
}

void dht_thread_rtable_deinit(struct dht_thread_rtable *t)
{
    dht_thread_deinit(&t->t);
}

static void* run(void *args)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht *dht = (struct dht *)args;

    int exit_needed = 0;

    while(!exit_needed)
    {
        struct dht_mqmsg *m_in = mq_wait(&dht->th_rtable.t.mq);

        switch(m_in->type) {
        case DHT_MQMSG_TYPE_FIND_NODE:
        {
            on_mqmsg_find_node(dht, m_in);
            break;
        }
        case DHT_MQMSG_TYPE_RTABLE_UPDATE:
        {
            on_mqmsg_rtable_update(dht, m_in);
            break;
        }
        case DHT_MQMSG_TYPE_RTABLE_FIND_K_CLOSEST:
        {
            on_mqmsg_rtable_find_k_closest(dht, m_in);
            break;
        }
        case DHT_MQMSG_TYPE_STOP:
        {
            on_mqmsg_stop(dht, m_in);
            exit_needed = 1;
            break;
        }
        default:
            break;
        }

        dht_mqmsg_delete(m_in);
    }

    pthread_exit(NULL);
}

static void on_mqmsg_find_node(struct dht *dht, struct dht_mqmsg *m_in)
{
    (void)(dht);
    (void)(m_in);
    DHT_LOG(LOG_DEBUG, "in");
}

static void on_mqmsg_rtable_update(struct dht *dht, struct dht_mqmsg *m_in)
{
    DHT_LOG(LOG_DEBUG, "in");
    struct dht_contact *c = (struct dht_contact *)m_in->data;
    rtable_update(&dht->rt, c);
}

static void on_mqmsg_rtable_find_k_closest(struct dht *dht, struct dht_mqmsg *m_in)
{
    DHT_LOG(LOG_DEBUG, "in");
    dht_nodeid_t *id = (dht_nodeid_t *)m_in->data;

    dht_clist_t contacts;
    dht_rtable_find_k_closest(&dht->rt, *id, contacts);

    struct dht_mqmsg *m_out = dht_mqmsg_new(DHT_MQMSG_TYPE_RTABLE_FIND_K_CLOSEST, contacts);
    mq_push(&dht->th_rtable.t.mq, m_out);
}

static void on_mqmsg_stop(struct dht *dht, struct dht_mqmsg *m_in)
{
    (void)(m_in);
    DHT_LOG(LOG_DEBUG, "in");
    struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_STOP, NULL);
    mq_push(&dht->mq, m);
}

void rtable_update(const struct dht_rtable *rt, const struct dht_contact *c)
{
    dht_nodeid_t xor;
    dht_nodeid_xor(rt->id, c->id, xor);
    int kb_idx = dht_nodeid_prefixLength(xor);

    struct dht_kbucket_node *kb_node = dht_kbucket_find(rt->kbuckets[kb_idx], c->id);
    if(kb_node != NULL)
    {
        // node is already in kbucket => move it to the front
        struct dht_kbucket_node *tmp = rt->kbuckets[kb_idx]->nodes->next;
        rt->kbuckets[kb_idx]->nodes->next = kb_node->next;
        kb_node->next = tmp;
        rt->kbuckets[kb_idx]->nodes = kb_node;
    }
    else
    {
        if(rt->kbuckets[kb_idx]->size < DHT_K_VALUE)
        {
            // kbucket is not full => add it to end
            struct dht_kbucket_node *i = rt->kbuckets[kb_idx]->nodes;
            if(i != NULL)
            {
                for(; i->next != NULL; i = i->next);
                i->next = dht_kbucket_node_new(c->id, &c->addr);
            }
            else
            {
                rt->kbuckets[kb_idx]->nodes = dht_kbucket_node_new(c->id, &c->addr);
            }
            rt->kbuckets[kb_idx]->size++;
        }
        else
        {
            printf("kbucket %d is full\n", kb_idx);
            // TODO: ping nodes starting at the end.
            //       if one node does not respond
            //          remove dead node and add the new one
            //       else
            //          discard new node

            // TODO: struct dht_kbucket_node has to be double linked
            for(struct dht_kbucket_node *i = rt->kbuckets[kb_idx]->nodes; i; i = i->next)
            {
                // won't work
                // this sends the request but the response will be received by rpc thread

                struct dht_mqmsg *m = dht_mqmsg_new(DHT_MQMSG_TYPE_PING, &i->c.addr);

                mq_push(&dht->th_rtable.t.mq, m);

                // waits for response message from rpc thread
                // the response is by guarantee by the rpc thread!
                // implement a timeout? how? where, here or in rpc thread?
                m = mq_wait_and_peek(&dht->th_rtable.t.mq);
                while(m->type != DHT_MQMSG_TYPE_PING)
                    m = mq_wait_and_peek(&dht->th_rtable.t.mq);


                int rc = dht_rpc_ping(&dht->rpc, dht->rt.id, &i->c.addr);
                if(rc == 0)
                {
                    printf("[%s:%d]is alive\n", i->contact.addr.ip, i->contact.addr.port);
                }
                else
                {
                    printf("[%s:%d]is not alive\n", i->contact.addr.ip, i->contact.addr.port);
                    break;
                }
            }
        }
    }
}
