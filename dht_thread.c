#include "dht_thread.h"

void dht_thread_init(struct dht_thread *t, void* (*mainfunc)(void*), void *args)
{
    mq_init(&t->mq);

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    pthread_create(&t->thread, &attr, mainfunc, args);

    pthread_attr_destroy(&attr);
}

void dht_thread_deinit(struct dht_thread *t)
{
    mq_delete(&t->mq);
}
