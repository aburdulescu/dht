#ifndef __DHT_CLIST__
#define __DHT_CLIST__

/**
 * @file clist.h
 */

#include "cmn.h"
#include "nodeid.h"
#include "contact.h"
#include "kbucket.h"

struct dht_clist_elem
{
    struct dht_contact contact;
    dht_nodeid_t distance;
};

/**
 * clist
 */
typedef struct dht_clist_elem dht_clist_t[DHT_K_VALUE];

#define DHT_CLIST_LENGTH DHT_K_VALUE
#define DHT_CLIST_SIZE (sizeof(struct dht_clist_elem) * DHT_K_VALUE)

struct json* dht_clist_dump(const dht_clist_t contacts);
void dht_clist_copy(dht_clist_t contacts, int *contacts_sz, struct dht_kbucket *kb, const dht_nodeid_t target);
int dht_clist_compare(const void *c0, const void *c1);
void dht_clist_sort(dht_clist_t contacts, int contacts_sz);

#endif
