#include <string.h>
#include <arpa/inet.h>

#include "cmn.h"
#include "ip4addr.h"


int dht_ip4addr_ntop(const struct sockaddr_in *n_addr, struct dht_ip4addr *p_addr)
{
    int rc = 0;

    memset(p_addr->ip, 0, DHT_IP4ADDR_LENGTH);

    if(inet_ntop(AF_INET, &n_addr->sin_addr, p_addr->ip, DHT_IP4ADDR_LENGTH) == NULL)
    {
        LOG_SYSCALL_ERROR("inet_ntop");
        rc = -1;
    }
    p_addr->port = ntohs(n_addr->sin_port);

    return rc;
}

int dht_ip4addr_pton(const struct dht_ip4addr *p_addr, struct sockaddr_in *n_addr)
{
    n_addr->sin_port = htons(p_addr->port);

    int rc = inet_pton(AF_INET, p_addr->ip, &n_addr->sin_addr);
    if(rc == 0)
    {
        printf("%s: bad IP address format\n", __FUNCTION__);
        return -1;
    }
    else if(rc == -1)
    {
        LOG_SYSCALL_ERROR("inet_pton");
        return -1;
    }

    return 0;
}

int dht_ip4addr_equals(const struct dht_ip4addr *addr0, const struct dht_ip4addr *addr1)
{
    int rc = addr0->port == addr1->port;
    if(rc == 0)
        return 0;

    rc = strcmp(addr0->ip, addr1->ip);
    if(rc != 0)
        return 0;

    return 1;
}

struct json* dht_ip4addr_dump(const struct dht_ip4addr *addr)
{
    struct json *root_obj = json_new(JSON_TYPE_OBJECT);

    // ip
    json_new_set_add(root_obj, JSON_TYPE_STRING, "ip", 2);
    json_new_set_add(root_obj, JSON_TYPE_STRING, (void *)addr->ip, sizeof(addr->ip));

    // port
    json_new_set_add(root_obj, JSON_TYPE_STRING, "port", 4);
    json_new_set_add(root_obj, JSON_TYPE_NUMBER, (void *)&addr->port, sizeof(addr->port));

    return root_obj;
}
