#ifndef __DHT_MQ__
#define __DHT_MQ__

/**
 * @file mq.h
 */

#include <pthread.h>
#include "queue.h"


struct mq
{
    pthread_cond_t cv;

    pthread_mutex_t q_mtx;
    pthread_mutex_t cv_mtx;

    struct queue *q;
};

void mq_init(struct mq *mq);
void mq_delete(struct mq *mq);
void mq_push(struct mq *mq, void *data);
int mq_is_empty(struct mq *mq);
void* mq_pop(struct mq *mq);
void* mq_wait_and_pop(struct mq *mq);
void* mq_wait_and_peek(struct mq *mq);

#endif
