#ifndef __DHT_LOG__
#define __DHT_LOG__

#include <stdarg.h>
#include <syslog.h>

#define DHT_LOG(level, ...) dht_log(level, __FILE__, __FUNCTION__, __VA_ARGS__)

void dht_log_open();
void dht_log_close();
void dht_log(int level, const char *file, const char *func, const char *format, ...);

#endif
