#include "clist.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "json.h"

struct json* dht_clist_dump(const dht_clist_t contacts)
{
    struct json *root_obj = json_new(JSON_TYPE_ARRAY);

    for(int i = 0; i < DHT_K_VALUE; ++i)
    {
        struct json *elem_obj = json_new(JSON_TYPE_OBJECT);

        // distance
        dht_nodeid_printable_t distance_printable;
        dht_nodeid_btop(contacts[i].distance, distance_printable);
        json_new_set_add(elem_obj, JSON_TYPE_STRING, "distance", 8);
        json_new_set_add(elem_obj, JSON_TYPE_STRING, distance_printable, sizeof(distance_printable));

        // contact
        json_new_set_add(elem_obj, JSON_TYPE_STRING, "contact", 7);
        json_add(elem_obj, dht_contact_dump(&contacts[i].contact));

        json_add(root_obj, elem_obj);
    }

    return root_obj;
}

void dht_clist_copy(dht_clist_t contacts, int *contacts_sz, struct dht_kbucket *kb, const dht_nodeid_t target)
{
    for(struct dht_kbucket_node *i = kb->nodes; i && *contacts_sz != DHT_K_VALUE; i = i->next)
    {
        memcpy(&contacts[*contacts_sz].contact, &i->contact, sizeof(struct dht_contact));

        dht_nodeid_t distance;
        dht_nodeid_xor(target, i->contact.id, distance);
        dht_nodeid_copy(contacts[*contacts_sz].distance, distance);

        (*contacts_sz)++;
    }
}

int dht_clist_compare(const void *c0, const void *c1)
{
    struct dht_clist_elem *c0_conv = (struct dht_clist_elem *)c0;
    struct dht_clist_elem *c1_conv = (struct dht_clist_elem *)c1;

    return dht_nodeid_compare(c0_conv->distance, c1_conv->distance);
}

void dht_clist_sort(dht_clist_t contacts, int contacts_sz)
{
    qsort(contacts, contacts_sz, sizeof(struct dht_clist_elem), dht_clist_compare);
}
