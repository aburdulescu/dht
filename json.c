#include "json.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack.h"


static void str_print(char **str, char *data);

static struct json* init_node(enum json_type t)
{
    struct json *j = (struct json *)malloc(sizeof(struct json));
    if(j == NULL)
        return NULL;

    memset(j, 0, sizeof(struct json));

    j->child = NULL;
    j->last_child = NULL;
    j->next = NULL;
    j->prev = NULL;
    j->type = t;
    j->child_num = 0;
    j->idx = 0;

    return j;
}

struct json* json_new(enum json_type t)
{
    struct json *jo = init_node(t);
    if(jo == NULL)
        return NULL;

    if(t == JSON_TYPE_ARRAY || t == JSON_TYPE_OBJECT)
    {
        struct json *jo_end = init_node(JSON_TYPE_END);
        if(jo_end == NULL)
            return NULL;

        jo->child = jo->last_child = jo_end;
    }

    return jo;
}

enum json_error json_set_value(struct json *jo, void *v, int v_sz)
{
    enum json_error rc = JSON_ERROR_OK;
    switch(jo->type) {
    case JSON_TYPE_OBJECT:
    case JSON_TYPE_ARRAY:
        rc = JSON_ERROR_WRONG_TYPE;
        break;
    case JSON_TYPE_STRING:
        jo->value.string = (char *)malloc(v_sz+1);
        memcpy(jo->value.string, v, v_sz);
        jo->value.string[v_sz] = 0;
        break;
    case JSON_TYPE_NUMBER:
        jo->value.number = *(int *)v;
        break;
    case JSON_TYPE_BOOL:
        jo->value.bool = *(enum json_bool *)v;
        break;
    case JSON_TYPE_NULL:
        jo->value.string = (char *)malloc(5);
        strcpy(jo->value.string, "null");
        jo->value.string[5] = 0;
        break;
    default:
        break;
    }

    return rc;
}

enum json_error json_add(struct json *root, struct json *jo)
{
    if(root == NULL)
    {
        printf("%s: root is NULL\n", __FUNCTION__);
        return JSON_ERROR_NULL;
    }
    if(jo == NULL)
    {
        printf("%s: jo is NULL\n", __FUNCTION__);
        return JSON_ERROR_NULL;
    }

    if(root->type != JSON_TYPE_OBJECT &&  root->type != JSON_TYPE_ARRAY)
    {
        printf("%s: need object or array\n", __FUNCTION__);
        return JSON_ERROR_WRONG_TYPE;
    }

    if(root->type == JSON_TYPE_OBJECT && root->child_num % 2 == 0 && jo->type != JSON_TYPE_STRING)
    {
        printf("%s: need a string\n", __FUNCTION__);
        return JSON_ERROR_WRONG_KEY_TYPE;
    }

    if(root->child->type == JSON_TYPE_END)
    {
        struct json *end = root->child;
        root->child = jo;
        root->last_child = jo;
        jo->next = end;
    }
    else
    {
        // preserve end node
        jo->next = root->last_child->next;;

        jo->prev = root->last_child;
        root->last_child->next = jo;
        root->last_child = jo;
    }

    root->last_child->idx = root->child_num++;

    return JSON_ERROR_OK;
}

enum json_error json_new_set_add(struct json *root, enum json_type t, void *v, int v_sz)
{
    struct json *jo = json_new(t);
    if(jo == NULL)
        return JSON_ERROR_NULL;

    enum json_error rc = JSON_ERROR_OK;

    rc = json_set_value(jo, v, v_sz);
    if(rc != JSON_ERROR_OK)
        return rc;

    rc = json_add(root, jo);
    if(rc != JSON_ERROR_OK)
        return rc;

    return rc;
}

void json_delete(struct json *root)
{
    if(root == NULL)
        return;

    struct stack *st = stack_new();
    stack_push(&st, root);

    struct json *j = NULL;

    while(!stack_is_empty(st))
    {
        j = stack_pop(&st);

        if(j->child != NULL)
        {
            stack_push(&st, j->child);
        }
        if(j->next != NULL)
        {
            stack_push(&st, j->next);
        }

        if((j->type == JSON_TYPE_STRING) || (j->type == JSON_TYPE_NULL))
            free(j->value.string);
        free(j);
    }

    stack_delete(st);
}

char* json_dump(struct json *root, int indent_size)
{
    if(root == NULL)
        return NULL;

    struct stack *st_prev = stack_new();
    struct stack *st_next = stack_new();

    stack_push(&st_next, root);

    struct json *j = NULL;

    int curr_indent_sz = 0;

    char *buf = NULL;

#define sindent() for(int i=0; i < curr_indent_sz; ++i) str_print(&buf, " ")
#define sindent_inc() curr_indent_sz += indent_size
#define sindent_dec() curr_indent_sz -= indent_size

    while(!stack_is_empty(st_next))
    {
        j = stack_pop(&st_next);

        switch(j->type) {
        case JSON_TYPE_OBJECT:
        {
            if(j->prev && j->prev->type != JSON_TYPE_STRING)
                sindent();
            str_print(&buf, "{\n");
            sindent_inc();
            break;
        }
        case JSON_TYPE_ARRAY:
        {
            str_print(&buf, "[\n");
            sindent_inc();
            break;
        }
        case JSON_TYPE_STRING:
        {
            if(j->idx % 2 == 0)
            {
                sindent();
                str_print(&buf, "\"");
                str_print(&buf, j->value.string);
                str_print(&buf, "\": ");
            }
            else
            {
                str_print(&buf, "\"");
                str_print(&buf, j->value.string);
                str_print(&buf, "\"");
                if(j->next != NULL)
                    str_print(&buf, ",\n");
                else
                    str_print(&buf, "\n");
            }
            break;
        }
        case JSON_TYPE_NUMBER:
        {
            char tmp[500];
            memset(tmp, 0, strlen(tmp));
            sprintf(tmp, "%d", j->value.number);
            str_print(&buf, tmp);
            if(j->next != NULL)
                str_print(&buf, ",\n");
            else
                str_print(&buf, "\n");
            break;
        }
        case JSON_TYPE_BOOL:
        {
            str_print(&buf, (j->value.bool == JSON_BOOL_FALSE) ? "false": "true");
            if(j->next != NULL)
                str_print(&buf, ",\n");
            else
                str_print(&buf, "\n");
            break;
        }
        case JSON_TYPE_NULL:
        {
            str_print(&buf, "null");
            if(j->next != NULL)
                str_print(&buf, ",\n");
            else
                str_print(&buf, "\n");
            break;
        }
        case JSON_TYPE_END:
        {
            struct json *tmp = stack_pop(&st_prev);
            sindent_dec();
            sindent();
            str_print(&buf, (tmp->type == JSON_TYPE_OBJECT) ? "}" : "]");
            if(j->next != NULL)
                str_print(&buf, ",\n");
            else
                str_print(&buf, "\n");
            break;
        }
        default:
            break;
        }

        if(j->next != NULL)
        {
            stack_push(&st_next, j->next);
        }

        if(j->type == JSON_TYPE_OBJECT || j->type == JSON_TYPE_ARRAY)
            stack_push(&st_prev, j);

        if(j->child != NULL)
        {
            stack_push(&st_next, j->child);
        }
    }

    stack_delete(st_next);

    return buf;
}

static void str_print(char **str, char *data)
{
    size_t data_sz = strlen(data);
    size_t str_sz = (*str == NULL) ? 0 : strlen(*str);
    size_t new_str_sz =  str_sz + data_sz;

    char *tmp = (char *)realloc(*str, new_str_sz + 1);

    if(str_sz == 0)
        memset(tmp, 0, new_str_sz);
    else
        memset(tmp+str_sz, 0, data_sz);

    strncat(tmp, data, data_sz+1);
    tmp[new_str_sz] = 0;
    *str = tmp;
}
