#ifndef __DHT_THREAD__
#define __DHT_THREAD__

#include "pthread.h"
#include "mq.h"


struct dht_thread
{
    /** pthread stucture */
    pthread_t thread;

    /** thread message queue */
    struct mq mq;
};

void dht_thread_init(struct dht_thread *t, void* (*mainfunc)(void*), void *args);
void dht_thread_deinit(struct dht_thread *t);

#endif
