#include "rpc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <assert.h>

#include "cmn.h"
#include "log.h"


int dht_rpc_open(struct dht_rpc *r, int port)
{
    r->fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (r->fd == -1)
    {
        LOG_SYSCALL_ERROR("socket");
        return -1;
    }

    int rc = 0;

    struct sockaddr_in sin;
    memset((char *) &sin, 0, sizeof(sin));

    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);
    sin.sin_addr.s_addr = htonl(INADDR_ANY);

    rc = bind(r->fd, (struct sockaddr*)&sin, sizeof(sin));
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("bind");
        close(r->fd);
    }

    struct sockaddr_in raw_addr;
    socklen_t raw_addr_len = sizeof(struct sockaddr_in);
    rc = getsockname(r->fd, (struct sockaddr *)&raw_addr, &raw_addr_len);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("getsockname");
        close(r->fd);
    }

    rc = dht_netaddr_ntop(&raw_addr, &r->addr);

    return rc;
}

void dht_rpc_close(const struct dht_rpc *r)
{
    DHT_LOG(LOG_DEBUG, "in");
    close(r->fd);
}

int dht_rpc_ping(const struct dht_rpc *r, const dht_nodeid_t sender, const struct dht_netaddr *to)
{
    struct dht_rpc_message m_req;
    memset(&m_req, 0, sizeof(m_req));
    m_req.type = DHT_RPC_MESSAGE_TYPE_PING_REQ;
    dht_nodeid_copy(m_req.sender, sender);

    int rc = dht_rpc_write(r, &m_req, to);
    if(rc == -1)
        return -1;

    struct dht_rpc_message m_rsp;
    struct dht_netaddr rsp_addr;
    rc = dht_rpc_read(r, &m_rsp, &rsp_addr);
    if(rc == -1)
        return -1;

    rc = dht_netaddr_equals(to, &rsp_addr);

    return (rc == 1) ? 0 : -1;
}

int dht_rpc_find_node(const struct dht_rpc *r,
                      const dht_nodeid_t sender,
                      const struct dht_netaddr *to,
                      const dht_nodeid_t id,
                      dht_clist_t contacts)
{
    struct dht_rpc_message m_req;
    memset(&m_req, 0, sizeof(m_req));
    m_req.type = DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ;
    dht_nodeid_copy(m_req.sender, sender);
    dht_nodeid_copy(m_req.data.id, id);

    int rc = dht_rpc_write(r, &m_req, to);
    if(rc == -1)
        return -1;

    struct dht_rpc_message m_rsp;
    memset(&m_rsp, 0, sizeof(m_rsp));

    rc = dht_rpc_read(r, &m_rsp, NULL);
    if(rc == -1)
        return -1;

    memcpy(contacts, m_rsp.data.contacts, DHT_CLIST_SIZE);

    return 0;
}

int dht_rpc_ping_rsp(const struct dht_rpc *r, const dht_nodeid_t sender, const struct dht_netaddr *to)
{
    struct dht_rpc_message m_rsp;
    memset(&m_rsp, 0, sizeof(m_rsp));
    m_rsp.type = DHT_RPC_MESSAGE_TYPE_PING_RSP;
    dht_nodeid_copy(m_rsp.sender, sender);

    int rc = dht_rpc_write(r, &m_rsp, to);
    if(rc == -1)
        return -1;

    return 0;
}

int dht_rpc_find_node_rsp(const struct dht_rpc *r,
                          const dht_nodeid_t sender,
                          const struct dht_netaddr *to,
                          dht_clist_t contacts)
{
    struct dht_rpc_message m_rsp;
    memset(&m_rsp, 0, sizeof(m_rsp));
    m_rsp.type = DHT_RPC_MESSAGE_TYPE_FIND_NODE_RSP;
    dht_nodeid_copy(m_rsp.sender, sender);
    memcpy(m_rsp.data.contacts, contacts, DHT_CLIST_SIZE);

    int rc = dht_rpc_write(r, &m_rsp, to);
    if(rc == -1)
        return -1;

    return 0;
}

// TODO: check read ret val for how much it was read(how to make sure the whole message was received?)
int dht_rpc_read(const struct dht_rpc *r, struct dht_rpc_message *m, struct dht_netaddr *from)
{
    struct dht_binary_data d;
    d.size = DHT_RPC_MESSAGE_MAX_LENGTH;
    d.value = malloc(d.size);

    struct sockaddr_in sin;
    socklen_t sin_len = sizeof(sin);
    int rc = recvfrom(r->fd, d.value, d.size, 0, (struct sockaddr *)&sin, &sin_len);
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("recvfrom");
        return -1;
    }

    if(rc != 0)
    {
        dht_rpc_message_unpack(&d, m);
        free(d.value);
    }

    if(from != NULL)
    {
        rc = dht_netaddr_ntop(&sin, from);
        return -1;
    }

    return rc;
}

int dht_rpc_write(const struct dht_rpc *r, const struct dht_rpc_message *m, const struct dht_netaddr *to)
{
    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));

    if(dht_netaddr_pton(to, &sin) == -1)
        return -1;

    struct dht_binary_data d;
    dht_rpc_message_pack(m, &d);

    int rc = sendto(r->fd, d.value, d.size, 0, (struct sockaddr*)&sin, sizeof(sin));
    if(rc == -1)
    {
        LOG_SYSCALL_ERROR("sendto");
        return -1;
    }

    return rc;
}

// PRIVATE FUNCTIONS

/*
  Message structure:

  | Type   | Sender    | Data size | Data value            |
  | 1 byte | 20 bytes | 8 bytes    | value of Data size |

 */
void dht_rpc_message_pack(const struct dht_rpc_message *m, struct dht_binary_data *d)
{
    int buf_sz = 1 + sizeof(m->sender) + DHT_RPC_MESSAGE_DATA_SIZE;

    unsigned char *buf;
    buf = (unsigned char *)malloc(sizeof(struct dht_rpc_message) + DHT_RPC_MESSAGE_DATA_SIZE);

    unsigned char *type_ptr = buf;
    unsigned char *sender_ptr = type_ptr + 1;
    unsigned char *data_size_ptr = sender_ptr + DHT_NODEID_LENGTH;
    unsigned char *data_value_ptr = data_size_ptr + DHT_RPC_MESSAGE_DATA_SIZE;

    *type_ptr = (unsigned char)m->type;
    dht_nodeid_copy(sender_ptr, m->sender);

    size_t data_size = 0;
    switch(m->type) {
    case DHT_RPC_MESSAGE_TYPE_PING_REQ:
    case DHT_RPC_MESSAGE_TYPE_PING_RSP:
    {
        memset(data_size_ptr, 0, DHT_RPC_MESSAGE_DATA_SIZE);
        break;
    }
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ:
    {
        data_size = sizeof(m->data.id);
        memcpy(data_size_ptr, &data_size, DHT_RPC_MESSAGE_DATA_SIZE);
        memcpy(data_value_ptr, m->data.id, data_size);
        break;
    }
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_RSP:
    {
        data_size = sizeof(m->data.contacts);
        memcpy(data_size_ptr, &data_size, DHT_RPC_MESSAGE_DATA_SIZE);
        memcpy(data_value_ptr, m->data.contacts, data_size);
        break;
    }
    }

    buf_sz += data_size;

    d->size = buf_sz;
    d->value = malloc(d->size);

    memcpy(d->value, buf, buf_sz);

    free(buf);
}

void dht_rpc_message_unpack(const struct dht_binary_data *d, struct dht_rpc_message *m)
{
    unsigned char *type_ptr = d->value;
    unsigned char *sender_ptr = type_ptr + 1;
    unsigned char *data_size_ptr = sender_ptr + DHT_NODEID_LENGTH;
    unsigned char *data_value_ptr = data_size_ptr + DHT_RPC_MESSAGE_DATA_SIZE;

    m->type = (enum dht_rpc_message_type)*type_ptr;
    dht_nodeid_copy(m->sender, sender_ptr);

    size_t data_size = 0;
    memcpy(&data_size, data_size_ptr, DHT_RPC_MESSAGE_DATA_SIZE);

    switch(m->type) {
    case DHT_RPC_MESSAGE_TYPE_PING_REQ:
    case DHT_RPC_MESSAGE_TYPE_PING_RSP:
        break;
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ:
        memcpy(m->data.id, data_value_ptr, data_size);
        break;
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_RSP:
        memcpy(m->data.contacts, data_value_ptr, data_size);
        break;
    }
}

struct json* dht_rpc_message_dump(const struct dht_rpc_message *m)
{
    struct json *root_obj = json_new(JSON_TYPE_OBJECT);

    json_new_set_add(root_obj, JSON_TYPE_STRING, "type", 4);
    json_new_set_add(root_obj, JSON_TYPE_NUMBER, (void *)&m->type, sizeof(m->type));

    json_new_set_add(root_obj, JSON_TYPE_STRING, "sender", 6);

    dht_nodeid_printable_t sender_printable;
    dht_nodeid_btop(m->sender, sender_printable);
    json_new_set_add(root_obj, JSON_TYPE_STRING, sender_printable, sizeof(sender_printable));

    json_new_set_add(root_obj, JSON_TYPE_STRING, "data", 4);

    switch(m->type) {
    case DHT_RPC_MESSAGE_TYPE_PING_REQ:
    case DHT_RPC_MESSAGE_TYPE_PING_RSP:
    {
        int tmp = 0;
        json_new_set_add(root_obj, JSON_TYPE_NUMBER, &tmp, sizeof(tmp));
        break;
    }
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_REQ:
    {
        dht_nodeid_printable_t data_printable;
        dht_nodeid_btop(m->data.id, data_printable);
        json_new_set_add(root_obj, JSON_TYPE_STRING, data_printable, sizeof(data_printable));
        break;
    }
    case DHT_RPC_MESSAGE_TYPE_FIND_NODE_RSP:
    {
        struct json *j = dht_clist_dump(m->data.contacts);
        json_add(root_obj, j);
        break;
    }
    }

    return root_obj;
}
