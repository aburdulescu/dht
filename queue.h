#ifndef __DHT_QUEUE__
#define __DHT_QUEUE__

/**
 * @file queue.h
 */


struct queue
{
    struct queue* next;
    void* data;
};

struct queue* queue_init();
void queue_delete(struct queue *st);
void queue_push(struct queue **st, void* data);
void* queue_pop(struct queue **st);
void* queue_peek(struct queue *st);
int queue_is_empty(struct queue *st);

#endif
