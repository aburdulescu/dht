#ifndef __DHT_JSON__
#define __DHT_JSON__

/**
 * @file json.h
 */

#include <stdio.h>

/**
 * json errors
 */
enum json_error
{
    /** everything went well */
    JSON_ERROR_OK,
    /** json object is NULL */
    JSON_ERROR_NULL,
     /** json object is not the one expected */
    JSON_ERROR_WRONG_TYPE,
     /** json object has a type that cannot be a key */
    JSON_ERROR_WRONG_KEY_TYPE,
};

/**
 * json boolean type
 */
enum json_bool
{
    JSON_BOOL_FALSE,
    JSON_BOOL_TRUE
};

/**
 * json object types
 */
enum json_type
{
    JSON_TYPE_OBJECT,
    JSON_TYPE_ARRAY,
    JSON_TYPE_STRING,
    JSON_TYPE_NUMBER,
    JSON_TYPE_BOOL,
    JSON_TYPE_NULL,
    JSON_TYPE_END
};

/**
 * json value
 */
union json_value
{
    int number;
    char *string;
    enum json_bool bool;
    void *null;
};

struct json
{
    struct json *child;
    struct json *last_child;
    struct json *next;
    struct json *prev;
    enum json_type type;
    union json_value value;
    int child_num;
    int idx;
};

/**
 * @brief Create a json object
 * @param t the type of json object to create
 * @return a json object
 */
struct json* json_new(enum json_type t);

/**
 * @brief Set value of json object(only valid for STRING, NUMBER, BOOL, NULL)
 * @param v the value of the object
 * @param v_sz the size of the value of the object
 * @return error code
 * @see enum json_error
 */
enum json_error json_set_value(struct json *jo, void *v, int v_sz);

/**
 * Add a new json object to a root json object
 */
enum json_error json_add(struct json *root, struct json *jo);

/**
 * Does everything that json_new, json_set_value and json_add at once
 */
enum json_error json_new_set_add(struct json *root, enum json_type t, void *v, int v_sz);

/**
 * Free json object(and all his children)
 */
void json_delete(struct json *root);

/**
 * Dump json content to to string
 * string must be initialized with NULL and free'd when not needed anymore
 */
char* json_dump(struct json *root, int indent_size);

#endif
