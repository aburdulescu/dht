#ifndef __DHT_KBUCKET__
#define __DHT_KBUCKET__

/**
 * @file kbucket.h
 */

#include "contact.h"

struct dht_kbucket_node
{
    struct dht_contact contact;
    struct dht_kbucket_node *next;
};

struct dht_kbucket
{
    struct dht_kbucket_node *nodes;
    int size;
};

struct dht_kbucket* dht_kbucket_new();
struct dht_kbucket_node* dht_kbucket_find(const struct dht_kbucket *kb, const dht_nodeid_t id);
struct dht_kbucket_node* dht_kbucket_node_new(const dht_nodeid_t id, const struct dht_netaddr *addr);

/**
 * Dump kbucket to JSON object
 */
struct json* dht_kbucket_dump(const struct dht_kbucket *kb);

#endif
